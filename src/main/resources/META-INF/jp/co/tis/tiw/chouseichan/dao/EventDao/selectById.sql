SELECT
  e.event_id,
  e.event_name,
  e.description,
  c.candidate_id,
  p.participant_id,
  p.participant_name,
  v.answer,
  p.comment
FROM
  event e
LEFT JOIN
  candidate c
ON
  e.event_id = c.event_id
LEFT JOIN
  vote v
ON
  c.candidate_id = v.candidate_id
LEFT JOIN
  participant p
ON
  v.participant_id = p.participant_id
WHERE
  e.event_id = /* eventId */1
ORDER BY
  p.participant_id