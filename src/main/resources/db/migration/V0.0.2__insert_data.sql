insert into public.event(event_id,event_name,description) values (1,'送別会','送別会やります。');
insert into public.candidate(candidate_id,event_id,date_time) values (1,1,'2019/03/18');
insert into public.candidate(candidate_id,event_id,date_time) values (2,1,'2019/03/19');
insert into public.candidate(candidate_id,event_id,date_time) values (3,1,'2019/03/20');
insert into public.participant(participant_id,participant_name,comment) values (1,'一郎','19日は参加できません。');
insert into public.participant(participant_id,participant_name,comment) values (2,'次郎','');
insert into public.vote(participant_id,candidate_id,answer) values (1,1,'○');
insert into public.vote(participant_id,candidate_id,answer) values (1,2,'×');
insert into public.vote(participant_id,candidate_id,answer) values (1,3,'△');
insert into public.vote(participant_id,candidate_id,answer) values (2,1,'△');
insert into public.vote(participant_id,candidate_id,answer) values (2,2,'○');
insert into public.vote(participant_id,candidate_id,answer) values (2,3,'×');
SELECT pg_catalog.setval(pg_get_serial_sequence('event', 'event_id'), MAX(event_id)) FROM public.event;
SELECT pg_catalog.setval(pg_get_serial_sequence('participant', 'participant_id'), MAX(participant_id)) FROM public.participant;
SELECT pg_catalog.setval(pg_get_serial_sequence('candidate', 'candidate_id'), MAX(candidate_id)) FROM public.candidate;